# Dataset of the Evolutionary Algorithm for Style Transfer

the software: https://gitlab.com/jbruyere/evolutionary-algorithm-for-style-transfer

This repository stores the dataset used by the software:  
- **artworks**: the original artworks used as input of the software
- **artworks_512x512**: the artworks normalized to 512x512 pixels
- **comparisons**: the artworks used during the Turing Test of Art of the project
- **generated**: random images generated using CPPN (initially, they aimed to be used as sofware input)
- .odt files are the references of the artworks used